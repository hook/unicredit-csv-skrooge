#!/usr/bin/env python3

# Copyright © 2016 Matija Šuklje <matija@suklje.name>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# SPDX-License-Identifier: GPL-3.0+

import csv

#
#  Clean up and separate the data into two tables, as the source
#  material is quite messy.
#

#  The ”encoding” parameter needs to be here, otherwise the Windows BOM
#  is read as a normal character.
with open('export.csv', 'r', newline='', encoding='utf-8-sig') \
     as unicredit_file, \
     open('account_info.csv', 'w') as account_file, \
     open('transactions.csv', 'w') as transactions_file:

    #  Save the account info data into 'account_info.csv'.
    #  Assuming the first bogus line was already manually deleted before.
    account_file.write(unicredit_file.readline())
    account_file.write(unicredit_file.readline())

    #  Save the transactions data into 'transactions.csv'
    #  and omitting any empty line that breaks the CSV.
    for line in unicredit_file:
        #  TODO: fix this ugly hack by ommitting the bogus line in a more
        #  efficient way.
        if line != ";\n":
            transactions_file.write(line)

#
#  Filter old CSV into a new CSV
#

with open('transactions.csv', 'r') as transactions_file, \
     open('account_info.csv', 'r') as account_file, \
     open('for_import_in_Skrooge.csv', 'w') as skrooge_file:

    transactions_reader = csv.DictReader(
        transactions_file, delimiter=';', quotechar='"')

    account_reader = csv.DictReader(
        account_file, delimiter=';', quotechar='"')

    #  Introduce field names that work with stock Skrooge settings.
    header = ["date", "account", "number", "mode", "payee", "comment",
              "amount", "unit"]

    writer = csv.DictWriter(
        skrooge_file, fieldnames=header, delimiter=';', quotechar='"',
        quoting=csv.QUOTE_ALL)

    #
    #  Extract the name of the account in question. Needed for the next step.
    #

    #  TODO: Find a more elegant way than iterating through a table of one row.
    for row in account_reader:
        account_title = (row['Ime računa'])

    #
    #  Gather and clean up all relevant transaction data and prepare it in a 
    #  format that is usable by Skrooge.
    #

    #  Write the table’s header.
    writer.writeheader()

    #  Write the table’s data.
    for row in transactions_reader:
        #  TODO:Perhaps put an `if` statement here to check if status=KNJIŽENO
        data = {
            'date': row['Datum valute'],
            'account': account_title,
            'number': row['Referenca'],
#            'mode': row['Tip naloga'], # Does not exist anymore
            'payee': row['Ime prejemnika'],
            'comment': row['Podrobnosti plačila'],
            'amount': row['Znesek'],
            'unit':  row['Valuta']}

        writer.writerow(data)

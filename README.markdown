# Introduction

A simple Python script to clean up banking statements exported from [UniCredit Bank][unicredit] as CSV.

The initial goal is to produce a valid CSV that can be usefully imported into [Skrooge][].

The script is tested on UniCredit Bank Slovenia, but likely other UniCredit banks use the same online banking solution.


# Status

It currently works and shouldn’t destroy any data (no guarantees, of course!) – it takes the CSV as you download it from the UniCredit online bank and outputs a CSV that you can import into Skrooge.

Tested on UniCredit Slovenia’s (as of 2021, new) online bank for normal current accounts and for Visa. More tests to come.

**NB:** This scripts assumes that you named your accounts the same both in Skrooge and your online bank.


# Usage

## Prepare Skrooge

In [Skrooge][] make sure that in _Settings_, under the _Import/Export_, _CSV_ tab you have the following enabled:

- Date format: _Automatic detection_
- _Automatic search of the header_
- _Automatic search of the columns_

![Skrooge settings for CSV import/export](./screenshots/Skrooge_settings.png)

## Prepare the file

1. in UniCredit Online Bank export bank transaction data as `.xls`
2. open the `.xls` in e.g. LibreOffice Calc and export it as `.csv` with delimiter set to `;`, quote character set to `"` and the quotation marks around every field enabled
3. delete whole first row (either already in LibreOffice or in a text editor)
4. save the result in `export.csv`

## Run the script

The script takes `export.csv` as you obtain it from the UniCredit online bank and produces a `for_import_in_Skrooge.csv` file that you can import into Skrooge.

Simply run `main.py` in the same folder as your `export.csv` resides in.


# License

This script is licensed under the [GPL-3.0+][gpl3] and uses [SPDX license identifiers][SPDX].


[unicredit]: https://www.unicreditbank.si/
[Skrooge]: https://skrooge.org/
[SPDX]: https://spdx.org/
[gpl3]: https://www.gnu.org/licenses/gpl-3.0.en.html
